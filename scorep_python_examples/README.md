# Score-P Python Examples

This folder contains Score-P Python Examples.

This very examples are without GPU usage to enable learning of every system.
However, some examples can be modified to run on a GPU.

## Prerequisits
Make sure to have Jupyterlab installed.

You need additional resources in addition to the ones mentioned in the [top level readme](../README.md).

Execute `00_prepare_envrionmnet.sh` to install all needed packages
- Installs required packages over `pip`
- Installs Miniconda 3

To only install the Score-P Python bindings, execute
```
pip install scorep
```

## Execution
See the [jupyter-notebook file](./00_tutorial_start.ipynb), which contains this tutorial.


