#!/bin/env bash
set -ex
# Setup Environment for Python Example

source ../scorep.rc # needed for scorep Python package

if [ ! -f $PWD/.venv/bin/activate ]; then
    echo "Installing Python pip and requirements."
    python3 -m venv .venv
    source $PWD/.venv/bin/activate
    python3 -m pip install -r requirements.txt
    python3 -m pip install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cpu
    deactivate    
else
    echo "Virtual Environment found under './venv/bin/activate'. Do nothing"
fi


if [ ! -d $PWD/miniconda3 ]; then
    echo "Installing Miniconda3"
    mkdir -p $PWD/miniconda3
    wget --quiet https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O $PWD/miniconda3/miniconda.sh
    bash $PWD/miniconda3/miniconda.sh -b -u -p $PWD/miniconda3
    rm -rf $PWD/miniconda3/miniconda.sh
else
    echo "Miniconda3 found under './miniconda3'. Do nothing."
fi

