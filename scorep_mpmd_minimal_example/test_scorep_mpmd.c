#include <stdio.h>
#include <mpi.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>    


#ifdef SCOREP_USER_ENABLE
#include <scorep/SCOREP_User.h>
#else
#define SCOREP_USER_REGION_BEGIN(...)
#define SCOREP_USER_REGION_END(...)
#define SCOREP_USER_REGION_DEFINE(...)
#define SCOREP_USER_PARAMETER_STRING(...)
#endif

/* msleep(): Sleep for the requested number of milliseconds. */
int msleep(long msec)
{
    struct timespec ts;
    int res;

    if (msec < 0)
    {
        errno = EINVAL;
        return -1;
    }

    ts.tv_sec = msec / 1000;
    ts.tv_nsec = (msec % 1000) * 1000000;

    do {
        res = nanosleep(&ts, &ts);
    } while (res && errno == EINTR);

    return res;
}

double
rand_time(void)
{
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    time_t t;
    srand((unsigned) time(&t) + rank + rand()%1000);
    double rand_time = ((double)rand()/(double)RAND_MAX) * 20. + 10;
    printf("%f\n", rand_time);
    return rand_time;
}

int func_common1(int rank) {
    msleep( rand_time() );
    return rank*rank;
}
int func_common2(int rank) {
    msleep( rand_time() );
    return rank+rank;
}

void func_A(MPI_Comm comm)
{
    int rank;
    MPI_Comm_rank(comm, &rank);

    int a = func_common1(rank);
    
    printf("Func A: %i\n", a);
}
void func_B(MPI_Comm comm)
{
    int rank;
    MPI_Comm_rank(comm, &rank);

    int a = func_common2(rank);
    printf("Func B: %i\n", a);
    
}
void func_C(MPI_Comm comm)
{
    int rank;
    MPI_Comm_rank(comm, &rank);

    int a = func_common1(rank);
    
    printf("Func C: %i\n", a);
}

int main()
{
    SCOREP_USER_REGION_DEFINE ( handle )
    SCOREP_USER_REGION_DEFINE ( handle_init )
    SCOREP_USER_REGION_DEFINE ( handle_in_loop )
    SCOREP_USER_REGION_DEFINE ( handle_outer_loop )
    MPI_Init(NULL,NULL);
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
        
    printf("Hello from Rank %i of size %i\n", rank, size);
    SCOREP_USER_REGION_BEGIN( handle_init, "init", 
                              SCOREP_USER_REGION_TYPE_PHASE 
                            )
    func_A(MPI_COMM_WORLD);
    func_B(MPI_COMM_WORLD);
    func_C(MPI_COMM_WORLD);
    
    SCOREP_USER_REGION_END(handle_init)

    
    int color = rank / 2;
    MPI_Comm new_comm;
    MPI_Comm_split(MPI_COMM_WORLD, color, rank, &new_comm);
    
    char new_name[80];
    sprintf(new_name, "new_communicator_%i", color);
    MPI_Comm_set_name(new_comm, new_name);
    
    for (int j = 0; j < 11 ; ++j)
    {
        SCOREP_USER_REGION_BEGIN( handle_outer_loop, "global-outer", 
                                  SCOREP_USER_REGION_TYPE_DYNAMIC 
                                  | SCOREP_USER_REGION_TYPE_LOOP
                                  | SCOREP_USER_REGION_TYPE_PHASE )
        switch (color)
        {
            case 0:
            {
                SCOREP_USER_REGION_BEGIN( handle, "color0-outer", 
                                          //~ SCOREP_USER_REGION_TYPE_PHASE
                                          SCOREP_USER_REGION_TYPE_COMMON
                                           )
                SCOREP_USER_PARAMETER_STRING("comm_name", new_name)
                for (int i = 0; i < 3 ; ++i) 
                {
                    SCOREP_USER_REGION_BEGIN( handle_in_loop, "color0", 
                                              SCOREP_USER_REGION_TYPE_DYNAMIC 
                                              //~ | SCOREP_USER_REGION_TYPE_LOOP 
                                              )
                    
                    func_A(new_comm);
                    
                    SCOREP_USER_REGION_END(handle_in_loop)
		    MPI_Barrier(new_comm);
                }
                SCOREP_USER_REGION_END(handle)

            }; break;
        case 1:
            {
        
                SCOREP_USER_REGION_BEGIN( handle, "color1-outer", 
                                          //~ SCOREP_USER_REGION_TYPE_PHASE
                                          SCOREP_USER_REGION_TYPE_COMMON
                                           )
                SCOREP_USER_PARAMETER_STRING("comm_name", new_name)
                for (int i = 0; i < 2 ; ++i)
                {
                    SCOREP_USER_REGION_BEGIN( handle_in_loop, "color1", 
                                              SCOREP_USER_REGION_TYPE_DYNAMIC
                                              //~ | SCOREP_USER_REGION_TYPE_LOOP
                                               )
                    func_B(new_comm);
                    
                    SCOREP_USER_REGION_END(handle_in_loop)
		    MPI_Barrier(new_comm);

                }
                SCOREP_USER_REGION_END(handle)
            }; break;
        case 2:
            {
                SCOREP_USER_REGION_BEGIN( handle, "color2-outer", 
                                          //~ SCOREP_USER_REGION_TYPE_PHASE 
                                          SCOREP_USER_REGION_TYPE_COMMON
                                          )
                SCOREP_USER_PARAMETER_STRING("comm_name", new_name)
                for (int i = 0; i < 4 ; ++i)
                {
                    SCOREP_USER_REGION_BEGIN( handle_in_loop, "color2", 
                                              SCOREP_USER_REGION_TYPE_DYNAMIC
                                              //~ | SCOREP_USER_REGION_TYPE_LOOP
                                               )
                    func_C(new_comm);
                    
                    SCOREP_USER_REGION_END(handle_in_loop)
		    MPI_Barrier(new_comm);

                }
                SCOREP_USER_REGION_END(handle)
            }; break;
        }

        SCOREP_USER_REGION_END(handle_outer_loop)
        
        MPI_Barrier(MPI_COMM_WORLD);
    }    
    
    MPI_Finalize();
    return 0;
}
