#!/bin/env bash

set -e

source ../scorep.rc
source ../cube.rc
source ../scalasca.rc

PREP="scorep --user" make -B 

export SCOREP_ENABLE_TRACING=True
export SCOREP_ENABLE_PROFILING=True
export SCOREP_EXPERIMENT_DIRECTORY=scorep_mpmd_example

mpirun -np 6 --use-hwthread-cpus ./test_scorep_mpmd

mpirun -np 6 scout.mpi ./scorep_mpmd_example/traces.otf2

square -v -s ./scorep_mpmd_example

cd ./scorep_mpmd_example || {echo "Cannot enter experitment directory"; exit 1 }

cube_merge summary.cubex \
           trace.cubex

#~ cube_merge profile.cubex \
           #~ trace.cubex

cd ..
