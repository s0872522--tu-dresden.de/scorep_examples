#!/bin/env bash
set -euo pipefail
set -x
# This script is intended to be run after the software has been downloaded and extracted.


# Function to display help message
show_help() {
  echo "Usage: $0 [options]"
  echo ""
  echo "Options:"
  echo "  --with-papi       Build with PAPI support."
  echo "  -h, --help        Show this help message."
}

# Parse command-line options
build_papi=false
while [[ "$#" -gt 0 ]]; do
  case "$1" in
    --with-papi) build_papi=true ;;
    -h|--help) show_help; exit 0 ;;
    *) echo "Unknown parameter passed: $1"; show_help; exit 1 ;;
  esac
  shift
done

echo "This setup is targeted at 'Open MPI'. "
echo "E.g. 'apt install openmpi-common libopenmpi-dev'"
echo "Press 'ENTER' to continue."
read -r

# Function to configure, make, and make install a software package
install_software_outofsource() {
  local sw_name="$1"
  local directory_name="$2"
  local configure_flags="$3"
  local install_prefix="$PWD/software/$sw_name"
  
  if [ -d ${install_prefix} ]; then
    echo "Skipping software installation. Already found at" ${install_prefix}
    return
  fi
  
  # Enter the directory
  pushd "$directory_name" || { echo "Cannot cd into $directory_name. Exiting."; exit 1; }

  # Prepare build directory
  rm -rf _build && mkdir -p _build
  pushd _build || { echo "Cannot cd into build directory. Exiting."; exit 1; }  # Ensure you enter the build directory

  
  # Prepare the install directory
  mkdir -p "$install_prefix"

  # Configure the software with provided flags
  echo "Configuring $sw_name..."
  if ! ../configure --prefix="$install_prefix" $configure_flags; then
    echo "Error configuring $sw_name. Exiting."
    exit 1
  fi

  # Build the software
  echo "Building $sw_name..."
  if ! make -j "$(( $(nproc) / 2))"; then
    echo "Error building $sw_name. Exiting."
    exit 1
  fi

  # Install the software
  echo "Installing $sw_name..."
  if ! make install; then
    echo "Error installing $sw_name. Exiting."
    exit 1
  fi

  # Leave the build directory
  popd
  popd

  # Create a sourceable .rc file
  local rc_file="$PWD/${sw_name}.rc"
  echo "export PATH=\"$install_prefix/bin:\$PATH\"" > "$rc_file"
#  echo "export LD_LIBRARY_PATH=\"$install_prefix/lib:\$LD_LIBRARY_PATH\"" >> "$rc_file"
  echo "Software $sw_name installed successfully. Source the $rc_file to update your PATH and LD_LIBRARY_PATH."
 
  echo "source $rc_file" >> "${PERFTOOLSRC}"
}

install_software_insource() {
  local sw_name="$1"
  local directory_name="$2"
  local configure_flags="$3"
  local install_prefix="$PWD/software/$sw_name"

  if [ -d ${install_prefix} ]; then
    echo "Skipping software installation. Already found at" ${install_prefix}
    return
  fi

  # Enter the directory
  pushd "$directory_name" || { echo "Cannot cd into $directory_name. Exiting."; exit 1; }

  # Prepare the install directory
  mkdir -p "$install_prefix"

  # Configure the software with provided flags
  echo "Configuring $sw_name..."
  if ! ./configure --prefix="$install_prefix" $configure_flags; then
    echo "Error configuring $sw_name. Exiting."
    exit 1
  fi

  # Build the software
  echo "Building $sw_name..."
  if ! make -j "$(( $(nproc) / 2))"; then
    echo "Error building $sw_name. Exiting."
    exit 1
  fi

  # Install the software
  echo "Installing $sw_name..."
  if ! make install; then
    echo "Error installing $sw_name. Exiting."
    exit 1
  fi

  # Leave the build directory
  popd

  # Create a sourceable .rc file
  local rc_file="$PWD/${sw_name}.rc"
  echo "export PATH=\"$install_prefix/bin:\$PATH\"" > "$rc_file"
 # echo "export LD_LIBRARY_PATH=\"$install_prefix/lib:\$LD_LIBRARY_PATH\"" >> "$rc_file"
  echo "Software $sw_name installed successfully. Source the $rc_file to update your PATH and LD_LIBRARY_PATH."

  echo "source $rc_file" >> "${PERFTOOLSRC}"
}


export PERFTOOLSRC=$PWD/performance-tools.rc

echo "" > "${PERFTOOLSRC}"

mkdir -p tools && pushd tools

# Define configure flags for each software package
papi_configure_flags=""
cube_configure_flags=""
scorep_configure_flags="--with-mpi=openmpi --without-shmem --enable-shared --with-libunwind=download --with-libbfd=download"
scalasca_configure_flags="--with-mpi=openmpi "

# Build PAPI only if --with-papi was specified
if [ "$build_papi" = true ]; then
  install_software_insource "papi" "papi-7.1.0/src" "$papi_configure_flags"
  # Update scorep_configure_flags to include PAPI
  scorep_configure_flags+="  --with-papi-header=$PWD/software/papi/include --with-papi-lib=$PWD/software/papi/lib "
fi

# Assuming the tarballs are already extracted in the current directory, and the directory names match the software name-version format
install_software_outofsource "cube"   "CubeBundle-4.8.2" "$cube_configure_flags"
install_software_outofsource "scorep" "scorep-8.4"       "$scorep_configure_flags"

# Source Score-P as it is needed for Scalasca installation and usage
# Make sure to source only if Score-P was installed successfully
if [ -f "$PWD/scorep.rc" ]; then
  source "$PWD/scorep.rc"
  install_software_outofsource "scalasca" "scalasca-2.6.1" "$scalasca_configure_flags"
else
  echo "Score-P environment setup file not found. Unable to install Scalasca."
  exit 1
fi

popd

echo "All software installed."
