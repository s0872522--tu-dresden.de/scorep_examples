#!/bin/env bash

VENV_NAME=$PWD/venv_extrap

if [ ! -f "${VENV_NAME}/bin/activate" ]; then
	python -m venv ${VENV_NAME}
fi

source "${VENV_NAME}/bin/activate"
python -m pip install extrap --upgrade

# Create a sourceable .rc file
rc_file="$PWD/extrap.rc"
echo "source ${VENV_NAME}/bin/activate" > "$rc_file"

echo "Software $sw_name installed successfully. Source the $rc_file enable the 'extrap' Python Enviornment."


