> **Status:** Actively maintained
# Score-P Examples

This repository contains several examples of Score-P application.


## Getting started
Clone this repos via
```
git clone https://gitlab.hrz.tu-chemnitz.de/s0872522--tu-dresden.de/scorep_examples.git
cd scorep_examples
git submodule update --init --recursive
```

This tutorials assume that following is available:

- Score-P under `./software/scorep/`. Sourcable with `./scorep.rc`
- Cube under `./software/cube/`. Sourceable with `./cube.rc`
- Scalasca `./software/scalasca`. Sourceable with `./scalasca.rc`
- Access to Vampir interactive evaluation of the traces

The installation of Score-P, Scalasca and Cube can be done manually or with these scripts.
- `./download_and_extract_all.sh`
- `./build_all.sh`

Additional dependencies are noted in each example.

Only Linux is supported, tested with Ubuntu 22.04.

## Description
Score-P has a rich oecosystem. This repository collects and demonstrates them.

## Usage
See the folders for examples.

## Contribution
Feel free to add your example via MR!

## Authors and acknowledgment
Maintained by Maximilian Sander
