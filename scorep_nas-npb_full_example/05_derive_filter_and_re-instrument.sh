#!/bin/bash
set -euxo pipefail

NPB_VERSION=3.4.2
SIZE='W'
CORES=4

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd ${SCRIPT_DIR}

wait_for_input() {
    if [ ${CHECK:-true} == true ] ; then
        echo "Press Enter to Continue"
        read -r
    fi
}

show_help() {
cat << EOF

Run Score-P with Instrumentation
---------------

This script is named - $0

To print help '$0 -h or --help'

tldr.: '$0 --no-check'

CLI arguments
    --no-check      Do not wait for user confirmation         

EOF
}

while [[ $# -gt 0 ]]; do
    case $1 in
        -h|-\?|--help)
            show_help    # Display a usage synopsis.
            exit
            ;;
        --no-check)
            CHECK=false
            ;;
        *)  # Default case
            die "ERROR: Unknown option '$1'"
            ;;
    esac
    shift
done



source "$PWD/../performance-tools.rc"

SUBTYPES=('full' 'simple')
EXE_POSTFIX=('mpi_io_full' 'mpi_io_simple')

NPB_ROOT=$PWD/NPB"${NPB_VERSION}"/NPB"${NPB_VERSION%.*}"-MPI


infile="$PWD/results_full_instrumentation.txt"
outfile="$PWD/results_filtered_instrumentation.txt"

pushd "$NPB_ROOT" || { echo "Error: Cannot enter ${NPB_ROOT}"; exit -1; }

rm -f "${NPB_ROOT}/bin/*"


for idx in "${!EXE_POSTFIX[@]}"; do
    SCOREP_EXPERIMENT_DIRECTORY=$PWD/scorep-bt.${SIZE}.x.${EXE_POSTFIX[idx]}
    pushd ${SCOREP_EXPERIMENT_DIRECTORY} || { echo "Error: Cannot enter ${SCOREP_EXPERIMENT_DIRECTORY}"; exit -1; }
    
    scorep-score profile.cubex
    
    wait_for_input
    
    scorep-score -g profile.cubex
    
    scorep-score -f initial_scorep.filter profile.cubex
    
    wait_for_input
    
    popd
    
    PREP="scorep --instrument-filter=${SCOREP_EXPERIMENT_DIRECTORY}/initial_scorep.filter --io=runtime:posix --thread=pthread" \
        make -j -B bt CLASS=${SIZE} SUBTYPE=${SUBTYPES[idx]}
    
done


cp ${infile} ${outfile}
echo "filtered_instrumentation" >> ${outfile}

for exe in "${EXE_POSTFIX[@]}"; do

    export SCOREP_EXPERIMENT_DIRECTORY=$PWD/scorep-bt.${SIZE}.x.${exe}_filtered
    export SCOREP_ENABLE_TRACING=1
    
    mpirun -np ${CORES} ${NPB_ROOT}/bin/bt.${SIZE}.x.${exe} > log.${exe}
    grep -P "I/O data rate |Time in seconds =" log.${exe} | \
        grep -oE "[0-9]+\.[0-9]+" | \
        paste - - | \
        awk -v prefix="bt.${SIZE}.x.${exe}" \
            '{printf "%-20s\tTime in seconds: %-10s\tI/O data rate (MB/sec): %-5s\n", prefix, $2, $1}' \
            >> ${outfile}
    
done

echo "Close the two Cube windows to continue!"
wait

