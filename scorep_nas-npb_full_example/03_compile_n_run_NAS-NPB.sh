#!/bin/bash
set -euxo pipefail

# Case Definition
NPB_VERSION=3.4.2
SIZE='W'
CORES=4

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd ${SCRIPT_DIR}


# Loop Definition
SUBTYPES=('full' 'simple' 'fortran' 'epio')
EXE_POSTFIX=('mpi_io_full' 'mpi_io_simple' 'fortran_io' 'ep_io')

# Auxilary Info
NPB_ROOT=$PWD/NPB"${NPB_VERSION}"/NPB"${NPB_VERSION%.*}"-MPI

outfile="$PWD/results_no_instrumentation.txt"
echo "no instrumentation" > ${outfile}


# Main Part
pushd "$NPB_ROOT" || { echo "Error: Cannot enter ${NPB_ROOT}"; exit -1; }

# -- Compilation
for subtype in "${SUBTYPES[@]}"; do
    make -B -j bt CLASS=${SIZE} SUBTYPE=${subtype}
done


# -- Execution
for exe in "${EXE_POSTFIX[@]}"; do
    mpirun -np ${CORES} ${NPB_ROOT}/bin/bt.${SIZE}.x.${exe} > log.${exe}
    
    # Extract Runtime Information
    grep -P "I/O data rate |Time in seconds =" log.${exe} | \
        grep -oE "[0-9]+\.[0-9]+" | \
        paste - - | \
        awk -v prefix="bt.${SIZE}.x.${exe}" '{printf "%-20s\tTime in seconds: %-10s\tI/O data rate (MB/sec): %-5s\n", prefix, $2, $1}' >> ${outfile}
done
