#!/bin/bash
set -euxo pipefail


# Case Definition
NPB_VERSION=3.4.2

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd ${SCRIPT_DIR}

if [ ! -f ${PWD}/NPB${NPB_VERSION}.tar.gz ]; then
    echo "Downloading NPB Benchmark ..."
    wget --quiet https://www.nas.nasa.gov/assets/npb/NPB${NPB_VERSION}.tar.gz
    echo "... done"
else
    echo "Skip download, NPB${NPB_VERSION}.tar.gz found." 
fi

if [ ! -d ${PWD}/NPB${NPB_VERSION} ]; then
    echo "Extracting NPB${NPB_VERSION}.tar.gz"
    tar -xzf ${PWD}/NPB${NPB_VERSION}.tar.gz
    echo "... done"
else
    echo "Skip extraction, directory NPB${NPB_VERSION} found."
fi
