#!/bin/bash
set -euxo pipefail

# Case Definition
NPB_VERSION=3.4.2

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd ${SCRIPT_DIR}

NPB_ROOT=$PWD/NPB"${NPB_VERSION}"/NPB"${NPB_VERSION%.*}"-MPI

pushd "$NPB_ROOT" || { echo "Error: Cannot enter ${NPB_ROOT}"; exit -1; }

cp config/NAS.samples/make.def.gcc_mpich config/make.def

sed -i -e 's|^MPIFC = |MPIFC = $(PREP) |' \
       -e 's|^FFLAGS\t= |FFLAGS  = -fallow-argument-mismatch -w |' \
    config/make.def
    
    cat config/make.def
popd



