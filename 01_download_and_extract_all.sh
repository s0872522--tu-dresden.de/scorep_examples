#!/bin/env bash

# Downloads and extracts dependencies if not already available
# The Versions can be changed at the very bottom.


download_and_extract() {
  local url_template="$1"
  local version="$2"
  local file_name
  local directory_name

  # Use a regex to split the version into major, minor, and patch numbers
  if [[ $version =~ ([0-9]+)\.([0-9]+)(\.([0-9]+))? ]]; then
    major_version=${BASH_REMATCH[1]}
    minor_version=${BASH_REMATCH[2]}
    patch_version=${BASH_REMATCH[4]} # This will be empty if there is no patch version
  fi

  # Construct the version string
  local version_string="${major_version}.${minor_version}"
  local version_string_full="${major_version}.${minor_version}${patch_version:+.${patch_version}}"

  local count=$(grep -o "{version}" <<<"$url_template" | wc -l)

  # Perform replacement based on the count of {version}
  if (( count > 1 )); then
    # If multiple, replace the first occurrence with [major.minor]
    local url=${url_template/\{version\}/$version_string}
    # And replace the next (last) occurrence with the full version string
    url=${url/\{version\}/$version_string_full}
  else
    # If only one, replace it with the full version string
    local url=${url_template//\{version\}/$version_string_full}
  fi
  # Extract the file name from the URL
  file_name=$(basename "$url")
  directory_name="${file_name%.tar.gz}"

  # Check if the tarball doesn't exist and download it
  if [ ! -f "$file_name" ]; then
    echo "Downloading ${file_name}..."
    if ! wget --quiet "$url"; then
      echo "Error downloading ${file_name}. Aborting."
      exit 1
    fi
  else
    echo "Tarball ${file_name} found."
  fi

  # Check if the source directory doesn't exist and extract the tarball
  if [ ! -d "$directory_name" ]; then
    echo "Extracting ${file_name}..."
    if ! tar -xzf "$file_name"; then
      echo "Error extracting ${file_name}. Aborting."
      exit 1
    fi
  else
    echo "Source directory ${directory_name} found."
  fi
}

mkdir -p tools && pushd tools

# Define URL templates for the different software packages
scalasca_url_template="https://apps.fz-juelich.de/scalasca/releases/scalasca/{version}/dist/scalasca-{version}.tar.gz"
cube_url_template="http://apps.fz-juelich.de/scalasca/releases/cube/{version}/dist/CubeBundle-{version}.tar.gz"
scorep_url_template="https://perftools.pages.jsc.fz-juelich.de/cicd/scorep/tags/scorep-{version}/scorep-{version}.tar.gz"
papi_url_template="https://icl.utk.edu/projects/papi/downloads/papi-{version}.tar.gz"

# Call the download and extract function for each software package with the respective version
download_and_extract "$papi_url_template" "7.1.0"
download_and_extract "$scalasca_url_template" "2.6.1"
download_and_extract "$cube_url_template" "4.8.2"
download_and_extract "$scorep_url_template" "8.4"

popd

echo "All downloads and extractions completed."
